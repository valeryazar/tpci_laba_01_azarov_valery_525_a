library ieee;
use ieee.std_logic_1164.all;

entity labawork_01 is

  port(
    X1, X2, X3, X4, X5 : in  std_logic;
    Y                  : out std_logic
  );

end entity;

architecture rtl of labawork_01 is

  type state_type is (s0, s1, s2, s3);

  signal state : state_type;

begin
  Y <= X1 and X2 and X3 and (not (X3) or (X4 and X5));
end rtl;