library ieee;
use ieee.std_logic_1164.all;

entity labawork_01_tb is
end entity;

architecture rtl of labawork_01_tb is

  component labawork_01 is

    port(
      X1, X2, X3, X4, X5 : in  std_logic;
      Y                  : out std_logic
    );
  end component;
  signal X1, X2, X3, X4, X5, Y : std_logic;
begin

  process
  begin
    X1 <= '0';
    wait for 50 ps;
    X1 <= '1';
    wait for 50 ps;
  end process;

  process
  begin
    X2 <= '0';
    wait for 100 ps;
    X2 <= '1';
    wait for 100 ps;
  end process;

  process
  begin
    X3 <= '0';
    wait for 200 ps;
    X3 <= '1';
    wait for 200 ps;
  end process;

  process
  begin
    X4 <= '0';
    wait for 400 ps;
    X4 <= '1';
    wait for 400 ps;
  end process;

  process
  begin
    X5 <= '0';
    wait for 800 ps;
    X5 <= '1';
    wait for 800 ps;
  end process;

  Test : labawork_01 port map(X1, X2, X3, X4, X5, Y);

end rtl;